// const admin = (function ($) {
//     $.gajax = req
//     $.config = {
//         favorite: '127.0.0.1:8080/favorite',
//     }
//     return $;
// })(window.admin || {});

layui.define(['layer'], function (exports) {
    var layer = layui.layer;
    var cache_data_group = 'FAVORITE_FRONT_CACHE_DB';
    var favorite_app_host = 'http://127.0.0.1:9000';

    /**
     * 封装原生的 AJAX 请求
     * 要求返回的数据类型为 json
     * 发送到服务器的数据类型为 application/json
     */
    function req(url, data, type, success, headerToken) {
        type = type.toUpperCase();
        let successCallback = success;
        let param = {
            url: url,
            type: type,
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            data: data,
            beforeSend: function (xhr) {
                // 发送请求前运行的函数。
                // 1. 给请求加上头部信息，比如 Token 鉴权
                // 2. 给出请求加载框 loading
                if (!headerToken) {
                    // 设置默认 token
                } else {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + headerToken);
                }
                layer.load(2);
                // xhr.setRequestHeader('Access-Control-Allow-Origin', '*')
                // xhr.setRequestHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE, PUT')
            },
            error: function (xhr, status, error) {
                // 如果请求失败要运行的函数。
                // console.log(xhr);
                // console.log(status);
                // console.log(error);
                layer.msg('服务器错误：' + xhr.responseJSON.error, {icon: 2, time: 1200});
            },
            success: function (result, status, xhr) {
                // 当请求成功时运行的函数。
                // res 为自定义返回格式的 json 数据
                // console.log(result);
                // console.log(status);
                successCallback(result);
            },
            complete: function (xhr, status) {
                // 请求完成时运行的函数（在请求成功或失败之后均调用，即在 success 和 error 函数之后）。
                // 1. 可以用来关闭请求时创建的资源，比如：loading
                layer.closeAll('loading');
            }
        };

        // 发送同步 ajax 请求
        param.async = false;

        $.ajax(param);
    }

    var obj = {
        gajax: req,
        // 缓存临时数据
        putTempData: function (key, value) {
            if (value) {
                layui.sessionData(cache_data_group, {key: key, value: value});
            } else {
                layui.sessionData(cache_data_group, {key: key, remove: true});
            }
        },
        // 移出临时数据
        removeTempData: function (key) {
            layui.sessionData(cache_data_group, {key: key, remove: true});
        },
        // 获取缓存临时数据
        getTempData: function (key) {
            return layui.sessionData(cache_data_group)[key];
        },
        // 弹出窗口
        showModal: function (event, title, path, data, finished) {
            if (event === 'add' || event === 'modify') {
                obj.putTempData('model-form-obj', data);
                obj.putTempData('lay-event', event);
                layer.open({
                    type: 1,
                    title: title,
                    anim: 4,
                    offset: '10%',
                    area: '700px',
                    skin: 'layui-layer-molv',
                    success: function (layero, index) {
                        $(layero).children('.layui-layer-content').load(path);
                    },
                    end: function () {
                        obj.removeTempData('model-form-obj');
                        obj.removeTempData('lay-event');
                        // 调用自己的回调函数
                        finished();
                    }
                });
            } else {
                console.log("暂不支持的事件！");
            }
        },
        config: {
            favorite: {
                backend_url: favorite_app_host + '/favorite',
                image_url: favorite_app_host + '/image',
                table_cache_name: 'model-form-obj',
                table_cache_event: 'lay-event',
            },

        },
    }
    exports('admin', obj);
})